# MammalWeb basic engagement metrics

> [R](https://en.wikipedia.org/wiki/R_(programming_language)) script to derive basic engagement metrics for the [MammalWeb](https://www.MammalWeb.org/) project.

This [git](https://en.wikipedia.org/wiki/Git) repository contains an [R](https://en.wikipedia.org/wiki/R_(programming_language)) script and associated [RStudio](https://en.wikipedia.org/wiki/RStudio) project file to compute basic engagement metrics for the [MammalWeb](https://www.MammalWeb.org/) project. This includes numerical and graphical output.

Importantly, this repository also contains the latest canonical list of MammalWeb engagement events, including (1) "intervention" events such as talks, media coverage, workshops, etc.; (2) newsletters; and (3) Tweets. If any of (1)~(3) are updated, please make sure they are reflected in their corresponding data files in this repository.

## Install

0. This script **requires** data files (specifically, `Animal.csv`, `Photo.csv`, `Site.csv`, and `Upload.csv`) exported from the MammalWeb data cleaning script, which can be found in [this repository](https://gitlab.com/mammalweb/mammalweb-data-clean). Please follow the instructions in that repository first. The script **also requires** the `Users` table from the MammalWeb database, exported as `Users.csv`.
1. Install the latest version of [R](https://en.wikipedia.org/wiki/R_(programming_language)). Version 3.5.2 has been tested to work.
2. Install the latest version of [RStudio](https://en.wikipedia.org/wiki/RStudio). At time of writing, an installer can be downloaded from [here](https://www.rstudio.com/products/rstudio/download/#download). Version 1.1.463 has been tested to work.
3. Download (or in git-speak, *[clone](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)*) this repository onto your computer. This step can also be done within RStudio (I think that is the best way), in which case please name the RStudio project after the name of the repository (i.e. `mammalweb-engagement-analyses`).
4. (optional) Within RStudio, install the packages listed below first (succesfully tested version numbers in parentheses). The script will try to install any that you haven't, but it helps to pre-install these packages before running the script.
```
tidyverse (1.2.1)
EnvStats (2.3.1)
ggmap (3.0.0)
ggthemes (4.1.0)
jsonlite (1.6)
leaflet (2.0.2)
lubridate (1.7.4)
rgdal (1.4-3)
rnrfa (1.5.0)
scales (1.0.0)
zoo (1.8-5)
```
5. Place `Users.csv`, and the cleaned data files `Animal.csv`, `Photo.csv`, `Site.csv`, and `Upload.csv` (from step 0) into the `data` directory.

## Usage

### Organisation of the R script

The main R script `engagement-analyses.R` is split into several sections. The beginning of each section is delineated by a header followed by a series of `---`s, such as:

```
#
# Load required packages -------------------------------------------------------
#
```

(there are some "sub" headers without the `---`s, which do not count as section deliminators)

Lines leading up to and including the first few sections, `Load required packages`, `Set constants`, and `Read data` must be run before anything else. You *should* then be able to run the subsequent sections (`User growth`, `Sequences uploaded`...) independently of each other. In other words, those sections do not all need to be run nor do they need to be run in order. If that doesn't work, then the script probably needs to be fixed somewhere to allow that. In addition, within RStudio, each section can be collapsed and hidden to ease navigation.

### Update engagement events data

The directory `data/events` contains *manually* tabulated/updated [CSV](https://en.wikipedia.org/wiki/CSV) files that list all MammalWeb engagement events with timestamps. I have not figured out a way to automate this process, so as new events, newsletters, or Tweets occur, the following files should be manually updated:

* `interventions.csv` - Events such as talks, workshops, media coverage, etc.
* `newsletter-archive.csv` - History of MammalWeb newsletters.
* `tweet.js` - @[MammalWeb](https://twitter.com/mammalweb) Tweets.

**How to update Tweet data (`tweet.js`)?**

1. Log into the @[MammalWeb](https://twitter.com/mammalweb) Twitter account.
2. Go the the account "Settings" page.
3. **DO NOT** click on the "Request your archive" button to get Twitter data, because it is not currently supported by this script. Instead, click on the "Your Twitter data" link on the left-hand side menu.
4. On the next page, scroll down to the "Download your Twitter data" section and click on the red "Download data" button.
5. Twitter will provide instructions on how to get the data file, which will be a [ZIP](https://en.wikipedia.org/wiki/Zip_(file_format)) file named like this "twitter-YYYY-MM-DD-*.zip". For example, `twitter-2019-03-05-d4d6ce1122ab0cf7570b3a4e024149ce9281b7bd981fb66d4f92e0261555f043.zip` was the file I requested on 5 March 2019.
6. Extract the file `tweet.js` from the ZIP file and place it in `data/events`.

### Running the script

1. Once done all the required files are in place, the file and directory structure should look like this (`surveypointsexcludinginaccessiblepoints.csv` contains coordinates for camera trapping sites from Sammy's summer 2018 Durham-wide survey, and `GS_users.csv` is the list of gold standard users on MammalWeb):

```
mammalweb-engagement-analyses
├── data
│   ├── events
│   │   ├── interventions.csv
│   │   ├── newsletter-archive.csv
│   │   └── tweet.js
│   ├── Animal.csv
│   ├── GS_users.csv
│   ├── Photo.csv
│   ├── README.txt
│   ├── Site.csv
│   ├── surveypointsexcludinginaccessiblepoints.csv
│   ├── Upload.csv
│   └── Users.csv
├── engagement-analyses.R
├── mammalweb-engagement-analyses.Rproj
└── README.md
```
2. At this point, the easiest way to run this script is to open the corresponding project file - `engagement-analyses.Rproj` - in RStudio, which would set the [working directory](https://support.rstudio.com/hc/en-us/articles/200711843-Working-Directories-and-Workspaces) appropriately.
3. **IMPORTANT**: Before running, please set all the variables in the `Set constants` section first. This is done by setting `START_DATE` and `END_DATE` to indicate the time period of interest; `GS_USERS` reads the list of gold standard users and usually don't need to be changed; and set `SAVE_FIGURES` to `TRUE` or `FALSE` depending on if [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) figure exports are needed.
4. Run everything up to, and including, the end of the `Read data` section.
5. Run other sections as needed.

**NOTES**:

* The section `Sequences/photos uploaded and classified` attempts to calculate the total number of camera trap days. Phil has a different way of doing this which produces a slightly different, and possibly more accurate, number. However, I have not had time to include it in this script yet.
* The `Trapper activity` section, other than plotting Trapper engagement over time, also produces maps of MammalWeb camera trapping sites:
    * This requires several mapping-related packages including `rnrfa`, `rgdal`, `leaflet`, and `ggmap`. Of those, `rgdal` is hard to successfully install on some computers. It sometimes helps to first install the [Geospatial Data Abstraction Library (GDAL)](https://en.wikipedia.org/wiki/GDAL) by downloading it from [here](https://trac.osgeo.org/gdal/wiki/DownloadingGdalBinaries) for your operating system. For [Apple](https://en.wikipedia.org/wiki/Apple_Inc.)'s [macOS](https://en.wikipedia.org/wiki/MacOS), GDAL can be downloaded from [here](https://www.kyngchaos.com/software/frameworks/).
    * Making these maps requires an Internet connection. Specifically, it is required by the `leaflet()` and `get_stamenmap()` functions used in the script.
* The last section, `Intervention effects`, is a bit more ad-hoc and is to look at before and after changes in engagement metrics in response to intervention events.

## Expected output

If the script ran in its entirety and without error, the following outputs would be expected:

* In the R console, there would be textual summaries such as "As of 2018-12-31 23:59:59 there are 489 active users, and 101 are Trappers." There will also be several warnings.
* Graphical output can be seen in the "Plots" and "Viewer" tabs in RStudio, including figures and maps.
* If `SAVE_FIGURES` is set to `TRUE`, the script will create a directory called `output` and place the following files in it:
```
├── output
│   ├── classification_intensity_monthly_plot.png
│   ├── classification_intensity_plot.png
│   ├── deployment_durations_plot.png
│   ├── interventions_comp_plot.png
│   ├── interventions_effects_nt_wr_ratios_plot.png
│   ├── interventions_effects_wk_plot.png
│   ├── photos_cumulative_plot.png
│   ├── sequences_cumulative_plot.png
│   ├── sequences_cumulative_ratio_plot.png
│   ├── sites_Durham_map.png
│   ├── sites_GB_map.png
│   ├── sites_monthly_plot.png
│   ├── spotter_classifications_plot.png
│   ├── spotter_days_plot.png
│   ├── upload_intensity_plot.png
│   └── users_registered_plot.png
```

Sometimes, there would also be a file `output/sites_to_check.csv` which is a list of problematic sites that either has no uploads, or coordinates that do not look like UK grid references.

In the end, the file and directory structure should look like this:

```
mammalweb-engagement-analyses
├── data
│   ├── events
│   │   ├── interventions.csv
│   │   ├── newsletter-archive.csv
│   │   └── tweet.js
│   ├── Animal.csv
│   ├── GS_users.csv
│   ├── Photo.csv
│   ├── README.txt
│   ├── Site.csv
│   ├── surveypointsexcludinginaccessiblepoints.csv
│   ├── Upload.csv
│   └── Users.csv
├── output
│   ├── classification_intensity_monthly_plot.png
│   ├── classification_intensity_plot.png
│   ├── deployment_durations_plot.png
│   ├── interventions_comp_plot.png
│   ├── interventions_effects_nt_wr_ratios_plot.png
│   ├── interventions_effects_wk_plot.png
│   ├── photos_cumulative_plot.png
│   ├── sequences_cumulative_plot.png
│   ├── sequences_cumulative_ratio_plot.png
│   ├── sites_Durham_map.png
│   ├── sites_GB_map.png
│   ├── sites_monthly_plot.png
│   ├── spotter_classifications_plot.png
│   ├── spotter_days_plot.png
│   ├── upload_intensity_plot.png
│   └── users_registered_plot.png
├── engagement-analyses.R
├── mammalweb-engagement-analyses.Rproj
└── README.md
```

## Contributing

Please send pull requests updating the files in `data/events` as events happen and changes to the script itself.

Small note: If editing this README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[GNU GPLv3 or later © Pen-Yuan Hsing](../LICENSE)