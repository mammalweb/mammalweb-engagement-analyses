The files in this directory are used by the R script to compute some basic stats on MammalWeb engagement.

`GS_users.csv` is the list of gold standard users on MammalWeb.

The files `Animal.csv`, `Photo.csv`, `Site.csv`, and `Upload.csv` need to be the cleaned versions produced by the MammalWeb data cleaning script in this repository:
https://gitlab.com/mammalweb/mammalweb-data-clean

`Users.csv` is exported directly from the MammalWeb MySQL database's `Users` table.

`surveypointsexcludinginaccessiblepoints.csv` contains the coordinates of camera trapping sites for Sammy's summer 2018 camera trap survey.

The `events` directory contains "intervention events" that we can try to measure engagement for, such as talks, workshops, Tweets, newsletters, etc. There are three files inside (except `tweet.js`, they are manually updated):

* `interventions.csv` - List of events like talks, media coverage, etc.
* `newsletter-archive.csv` - List of MammalWeb newsletters.
* `tweet.js` - List of @MammalWeb Tweets. This file is exported as part of Twitter's data export function. See this repository's main README.md for details.